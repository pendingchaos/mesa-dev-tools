# download_mr_version/compare_mr_versions

Downloads a version of a MR and compares it against another. This ignore commits added by a rebase
and shows added/removed commits, but reading a diff of two diffs can be a bit confusing.

You should ensure that your origin/main branch is up to date. Use --mesa_dir or adjust
download_mr_version if ../mesa/ (relative to the script's directory) doesn't exist.

```
./download_mr_version /tmp/before <username/mr-id> <before_commit>
./download_mr_version /tmp/after <username/mr-id> <after_commit (optional if mr-id is used)>
./compare_mr_versions /tmp/{before,after}
```

# mesa

Tool for building and running custom Mesa builds, with automatic prefix creation and reconfiguration.

## Build/run
```
mesa build --buildtype ... --commit ... --config ... <prefix>
mesa run --buildtype ... --commit ... --config ... <command>
mesa run --prefix ... <command>
```

If --buildtype is omitted, the default build type from `mesa-build/config.py` is used (which is debug).

If --commit is omitted, the tool builds from the main repo (path specified in `mesa-build/config.py`).
If '/' is in the commit, it is treated as `remote/branch` or `remote/branch/commit`.

--config can be any config from `mesa-build/config.py` (such as "radv" or "full"). If it's omitted,
the default from `mesa-build/config.py` is used (which is "radv").

If you don't give `mesa run` and `mesa build` a prefix, a temporary installation directory will be
used in `mesa-build/tmp/` which may overwritten after the command has finished.

This handles several concurrent `mesa run` and `mesa build` commands, even with different commits,
build types and build configs. Builds are serialized and additional prefixes are created/re-used.

The contents of `mesa-build/tmp/` (prefixes and build directories) may be deleted with "mesa clean".

The build directories for the main repository are `mesa-build/tmp/build/<buildtype>/MAIN_REPO/build/`.
You can create a symbolic link to conveniently access them to run tests.

## Add repositories as a source for commits
```
mesa add-repo ~/mesa/
# Usage: mesa run --commit <commit sha> ...
```

This won't modify the repository at all, but it will create a new directory in `mesa-build/repos/`
linked to the given repository which will be used for by --commit to obtain the tree for a particular
commit.

## Add remotes
```
mesa add-remote fdo git@gitlab.freedesktop.org:mesa/mesa
# Usage: mesa run --commit fdo/main ...
#        mesa run --commit fdo/main/<commit sha> ...
```

# foz

Tool for replaying specific pipelines from a Fossilize database. This compiles Mesa itself, similar
to the `mesa` command.

```
foz <pipeline0> ... <pipelineN>
foz -c <commit0> ... <commitN> -- <pipeline0> ... <pipelineN>
```

Pipelines are in the form database/hash, database/hash/stage or database/hash/stage/index. The
database is searched for in the directory specified by --fossils_dir by finding a filename which
starts with the database name. If it's not found, the current working directory is then used instead.

If more than one commit is given, it replays the pipelines for each commit and writes the output to
`/tmp/compare_foz/`. You can then compare them using a diff program.

This command sets RADV_DEBUG=nocache and RADV_PERFTEST to enable features which might be disabled by
default.

Defaults for the --fossils_dir and --fossilize_replay options are in `mesa-build/config.py`.
