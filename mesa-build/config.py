MESA_DIR = BASE_DIR / '../../mesa'
LLVM_BUILD_DIR = BASE_DIR / '../../llvm-project/llvm/build/'
LLVMBIN_DIR = BASE_DIR / '../../llvm_binaries/'

def inherit(base, ext):
   if base == None:
      return ext

   assert(type(base) == type(ext))
   if type(base) == dict:
      res = {k:v for k,v in base.items()}
      res.update({k:inherit(base.get(k, None), v) for k,v in ext.items()})
      return res
   elif type(base) == str:
      return base + ' ' + ext
   elif type(base) == list:
      return base + ext

def san(base):
   return inherit(base, {
      'run_env': [{
         'LD_PRELOAD': '/usr/lib64/libasan.so.8',
         'ASAN_OPTIONS': 'halt_on_error=1:abort_on_error=1:print_summary=1',
         'UBSAN_OPTIONS': 'halt_on_error=1:abort_on_error=1:print_summary=1:print_stacktrace=1',
      }],
      'setup_args': '-Db_sanitize=address,undefined',
   })

def clang(base):
   cross_file = BASE_DIR / 'cross-clang'
   if not cross_file.exists():
      cross_file.write_text(f'''[binaries]
c = '/usr/bin/clang'
cpp = '/usr/bin/clang++'
ar = '/usr/bin/llvm-ar'
strip = '/usr/bin/llvm-strip'
pkgconfig = '/usr/bin/pkg-config'
llvm-config = '/usr/bin/llvm-config'
''')

   return inherit(base, {
      'setup_args': f'--cross-file {cross_file}',
   })

c['main'] = str(MESA_DIR)
c['fossils'] = str(BASE_DIR / '../../radv_fossils/fossils')
c['fossilize_replay'] = str(BASE_DIR / '../../Fossilize/build/cli/fossilize-replay')

c['default_buildconfig'] = 'radv'
c['max_num_prefixes'] = 64
build = c['build_configs'] = {}

base_nomold = {
   'setup_args': '-Dllvm=enabled -Dselinux=true -Dbuild-aco-tests=true -Dvulkan-layers=device-select,overlay -Dtools=drm-shim',
   'default_buildtype': 'debug',
}

base = inherit(base_nomold, {
   'setup_args': '-Dc_link_args="-B/usr/libexec/mold/" -Dcpp_link_args="-B/usr/libexec/mold/"',
})

build['cl'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers= -Dgallium-opencl=icd -Dopencl-spirv=true -Dbuild-tests=false -Dbuild-aco-tests=false',
})

build['cl-lvp'] = inherit(base, {
   'env': [{
      'LP_CL': '1',
   }],
   'setup_args': '-Dgles1=disabled -Dgles2=disabled -Dopengl=false -Dglx=disabled -Degl=disabled -Dgbm=disabled -Dvulkan-drivers= -Dgallium-drivers=swrast -Dgallium-opencl=icd -Dopencl-spirv=true -Dbuild-tests=false -Dbuild-aco-tests=false',
})

build['radv'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers=amd -Dgallium-drivers= -Dopengl=false -Dgles1=disabled -Dgles2=disabled -Degl=disabled -Dbuild-tests=false',
})

build['radv-x86'] = inherit(base_nomold, {
   'setup_args': f'--cross-file {BASE_DIR / "cross-x86"} -Dvulkan-drivers=amd -Dgallium-drivers= -Dopengl=false -Dgles1=disabled -Dgles2=disabled -Degl=disabled -Dbuild-tests=false',
})

build['radv-lto'] = inherit(build['radv'], {
   'setup_args': '-Db_lto=true',
})

build['radv-tests'] = inherit(build['radv'], {
   'setup_args': '-Dbuild-tests=true',
})

build['radv-nollvm'] = inherit(build['radv'], {
   'setup_args': '-Dbuild-aco-tests=false -Dllvm=disabled',
})

build['amd'] = inherit(base, {
   'setup_args': '-Dbuild-tests=true -Dgallium-drivers=radeonsi -Dvulkan-drivers=amd',
})

for llvm_ver in [11, 12, 13, 14, 15, 16, 17, 18, 19, 'dev']:
   cross_file = BASE_DIR / f"cross-llvm{llvm_ver}"
   if not cross_file.exists():
      cross_file.write_text(f'''[binaries]
c = '/usr/bin/gcc'
cpp = '/usr/bin/g++'
ar = '/usr/bin/gcc-ar'
strip = '/usr/bin/strip'
pkgconfig = '/usr/bin/pkg-config'
llvm-config = '{LLVMBIN_DIR / f'llvm{llvm_ver}/release64/bin/llvm-config'}'
''')

   build[f'radv-llvm{llvm_ver}'] = inherit(build['radv'], {
      'setup_args': f'--cross-file {cross_file} -Dcpp_rtti=false',
   })

   build[f'amd-llvm{llvm_ver}'] = inherit(build['amd'], {
      'setup_args': f'--cross-file {cross_file} -Dcpp_rtti=false',
   })

build['lvp'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers=swrast -Dgallium-drivers=swrast -Dbuild-tests=false -Dbuild-aco-tests=false',
})

build['turnip'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers=freedreno -Dgallium-drivers= -Dopengl=false -Dgles1=disabled -Dgles2=disabled -Degl=disabled -Dbuild-tests=false -Dbuild-aco-tests=false -Dtools=drm-shim,freedreno',
})

build['full'] = inherit(base, {
   'setup_args': '-Dbuild-tests=true',
})

build['all'] = inherit(base, {
   'setup_args': '-Dbuild-tests=true -Dvulkan-drivers=all -Dgallium-drivers=all',
})

build['drmshim'] = inherit(base, {
   'setup_args': '-Dbuild-tests=true -Dtools=drm-shim,intel',
})

build['anv'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers=intel -Dgallium-drivers= -Dopengl=false -Dgles1=disabled -Dgles2=disabled -Degl=disabled -Dbuild-tests=false -Dtools=drm-shim,intel -Dbuild-aco-tests=false',
})

build['nirtest'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers= -Dgallium-drivers= -Dopengl=false -Dgles1=disabled -Dgles2=disabled -Degl=disabled -Dbuild-aco-tests=false -Dbuild-tests=true',
})

build['zink'] = inherit(base, {
   'env': [{
      'MESA_LOADER_DRIVER_OVERRIDE': 'zink',
   }],
   'setup_args': '-Dgallium-drivers=zink -Dvulkan-drivers=amd -Dbuild-tests=false',
})

build['zink-lvp'] = inherit(base, {
   'env': [{
      'MESA_LOADER_DRIVER_OVERRIDE': 'zink',
   }],
   'setup_args': '-Dgallium-drivers=zink,swrast -Dvulkan-drivers=swrast -Dbuild-tests=false -Dbuild-aco-tests=false',
})

build['softpipe'] = inherit(base, {
   'setup_args': '-Dvulkan-drivers= -Ddraw-use-llvm=false -Dbuild-aco-tests=false',
})

build['lvp-san'] = san(build['lvp'])
build['radv-san'] = san(build['radv'])
build['full-san'] = san(build['full'])
build['nirtest-san'] = san(build['nirtest'])

build['radv-clang'] = clang(build['radv'])
build['full-clang'] = clang(build['full'])
