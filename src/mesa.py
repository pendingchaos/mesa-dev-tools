import os
import hashlib
import subprocess
import shutil
import sys
import time
from pathlib import Path
from src.config import *

# files and directories
REPO_DIR = BASE_DIR / 'repos'
TMP_DIR = BASE_DIR / 'tmp'

MAIN_REPO = config['main']
DEFAULT_CONFIG = config['default_buildconfig']
MAX_NUM_PREFIXES = config['max_num_prefixes']

class BuildConfig:
   def __init__(self, setup_args, default_buildtype):
      self.build_env = {k:v for k,v in os.environ.items()}
      self.build_env_key = {}
      self.run_env = {k:v for k,v in os.environ.items()}
      self.setup_args = setup_args
      self.default_buildtype = default_buildtype

   def _update_env(self, env, src):
      for k, v in src.items():
         if k in env and k in ['LD_LIBRARY_PATH', 'PKG_CONFIG_PATH', 'PATH']:
            env[k] = str(v) + ':' + env[k]
         else:
            env[k] = str(v)

   def update_env(self, src):
      self._update_env(self.build_env, src)
      self._update_env(self.build_env_key, src)
      self._update_env(self.run_env, src)

   def update_run_env(self, src):
      self._update_env(self.run_env, src)

   def get_key(self):
      return repr((self.build_env_key, self.setup_args))

BUILD_CONFIGS = {}
for name, val in config['build_configs'].items():
   meson = val.get('setup_args', '')
   meson = [arg for arg in meson.split(' ') if arg != '']
   cfg = BUILD_CONFIGS[name] = BuildConfig(meson, val['default_buildtype'])

   for env in val.get('env', []):
      cfg.update_env(env)

   for env in val.get('run_env', []):
      cfg.update_run_env(env)

   BUILD_CONFIGS[name] = cfg

def sha256(data):
   m = hashlib.sha256()
   m.update(bytes(data))
   return m.hexdigest()[:16]

def write_file(fname, data):
   Path(fname).parent.mkdir(parents=True, exist_ok=True)

   with open(fname, 'w') as f:
      f.write(str(data))

def read_file(fname):
   Path(fname).parent.mkdir(parents=True, exist_ok=True)

   try:
      with open(fname, 'r') as f:
         return f.read();
   except FileNotFoundError:
      return ''

def rmtree(path):
   if path.exists():
      shutil.rmtree(path)

def clean():
   rmtree(TMP_DIR)

def run_quiet(cmd, **kwargs):
   check = kwargs.get('check', True)
   kwargs['check'] = False

   if kwargs.get('capture_output', False) or not os.isatty(sys.stdout.fileno()):
      kwargs['capture_output'] = False
      proc = subprocess.run(cmd, **kwargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
   else:
      proc = subprocess.run(cmd, **kwargs, capture_output=True, encoding='utf-8')

   if check and proc.returncode != 0:
      print('Command failed: %s' % ' '.join(str(v) for v in cmd), file=sys.stderr)
      print(proc.stdout, file=sys.stderr)
      sys.exit(1)

   return proc.stdout

def add_repo(directory):
   src_directory = Path(directory).absolute()
   dst_directory = REPO_DIR / sha256(src_directory)
   dst_directory.mkdir(parents=True, exist_ok=True)
   run_quiet(['git', 'clone', '--mirror', '--no-checkout', '--reference', src_directory, 'file://'+str(src_directory), dst_directory / '.git'])
   run_quiet(['git', 'config', '--bool', 'core.bare', 'false'], cwd=dst_directory)

def add_remote(name, url):
   assert(name != 'origin')

   if REPO_DIR.exists():
      for repo in REPO_DIR.iterdir():
         run_quiet(['git', 'remote', 'add', name, url], cwd=repo)

def acquire_lock(fname):
   fname.parent.mkdir(parents=True, exist_ok=True)

   fd = open(fname, 'w')
   os.lockf(fd.fileno(), os.F_LOCK, 0)
   return fd

def try_acquire_lock(fname):
   fname.parent.mkdir(parents=True, exist_ok=True)

   fd = open(fname, 'w')
   try:
      os.lockf(fd.fileno(), os.F_TLOCK, 0)
      return fd
   except:
      fd.close()
      return None

class Prefix:
   def __init__(self, name):
      self.dir = TMP_DIR / 'prefix' / name
      self.lock_fname = TMP_DIR / 'prefix' / ('lock_' + name)
      self.last_use_fname = TMP_DIR / 'prefix' / ('last_use_time_' + name)
      self.key_fname = TMP_DIR / 'prefix' / ('key_' + name)

      self.lock_fd = None
      self.last_use_time = None
      self.key = None

   def lock(self):
      self.lock_fd = try_acquire_lock(self.lock_fname)

      if self.lock_fd:
         tm = read_file(self.last_use_fname)
         self.last_use_time = float(tm) if tm != '' else 0

         self.key = read_file(self.key_fname)

      return self.lock_fd != None

   def unlock(self):
      self.lock_fd.close()
      self.lock_fd = None

   def get_last_use_time(self):
      assert(self.lock_fd)
      return self.last_use_time

   def update_last_use_time(self):
      assert(self.lock_fd)
      self.last_use_time = time.time()
      write_file(self.last_use_fname, str(self.last_use_time))

   def get_key(self):
      assert(self.lock_fd)
      return self.key

   def set_key(self, key):
      assert(self.lock_fd)
      write_file(self.key_fname, key)
      self.key = key

   @staticmethod
   def create():
      name = sha256(open('/dev/urandom', 'rb').read(32))
      prefix_dir = TMP_DIR / 'prefix' / name
      prefix_dir.mkdir(parents=True, exist_ok=True)
      pfx = Prefix(name)
      return pfx if pfx.lock() else None

class UnmanagedPrefix:
   def __init__(self, directory):
      self.dir = Path(directory)
      self.key = ''

   def lock(self):
      return true

   def unlock(self):
      pass

   def get_key(self):
      return self.key

   def set_key(self, key):
      self.key = key

def find_prefix(key, build_dir, prefer_match=True):
   prefix_base_dir = TMP_DIR / 'prefix'
   prefix_base_dir.mkdir(parents=True, exist_ok=True)

   best = None
   incomplete = None
   prefixes = list(prefix_base_dir.iterdir())
   for prefix_dir in prefixes:
      if not prefix_dir.is_dir():
         continue

      prefix = Prefix(prefix_dir.name)
      if not prefix.lock():
         continue

      if prefix.get_key() == '' and best == None:
         # Reserve an empty prefix (from a failed build), which we can use instead of creating a new one
         incomplete = prefix
         continue

      if prefer_match and prefix.get_key() != key:
         prefix.unlock()
         continue

      if best != None and prefix.get_key() == key and str(prefix.dir) != str(build_dir.get_prefix()):
         prefix.unlock()
         continue
      if best != None and prefix.get_key() != key and prefix.get_last_use_time() > best.get_last_use_time():
         prefix.unlock()
         continue

      if best:
         best.unlock()
      if incomplete:
         incomplete.unlock()
         incomplete = None
      best = prefix

   if best != None:
      best.update_last_use_time()
      return best

   if incomplete:
      return incomplete

   if prefer_match and len(prefixes) >= MAX_NUM_PREFIXES:
      return find_prefix(key, build_dir, False)

   prefix = Prefix.create()
   if not prefix:
      return find_prefix(key, build_dir, False)

   prefix.update_last_use_time()
   return prefix

class BuildDir:
   def __init__(self, buildtype, repo_name):
      base_path = TMP_DIR / 'build' / buildtype / repo_name
      self.directory = base_path / 'build'
      self.lock_fname = base_path / 'lock'
      self.config_fname = base_path / 'config'
      self.prefix_fname = base_path / 'prefix'

      self.lock_fd = None
      self.config = None
      self.prefix = None

   def get_config(self):
      assert(self.lock_fd)
      return self.config

   def get_prefix(self):
      assert(self.lock_fd)
      return self.prefix

   def set_config(self, config):
      assert(self.lock_fd)
      write_file(self.config_fname, config)
      self.config = config

   def set_prefix(self, prefix):
      assert(self.lock_fd)
      write_file(self.prefix_fname, prefix)
      self.prefix = Path(prefix)

   def lock(self):
      self.lock_fd = acquire_lock(self.lock_fname)
      self.config = read_file(self.config_fname)
      self.prefix = read_file(self.prefix_fname)

   def unlock(self):
      self.lock_fd.close()
      self.lock_fd = None

class Options:
   def __init__(self, buildtype=None, commit=None, config=DEFAULT_CONFIG, quiet=False, prefix=None):
      self.buildtype = buildtype
      self.commit = commit
      self.config = config
      self.quiet = quiet
      self.prefix = prefix

def build_mesa(options):
   config = BUILD_CONFIGS[options.config]
   buildtype = options.buildtype or config.default_buildtype

   # find repo with the commit
   remote = None
   commit = options.commit
   if commit and '/' in commit:
      # remote/branch
      # remote/branch/commit
      commit = commit.split('/')
      remote = commit[:-1] if len(commit) > 2 else commit
      commit = commit[-1] if len(commit) > 2 else 'FETCH_HEAD'

   if commit == None:
      repo_dir = MAIN_REPO
      repo_name = 'MAIN_REPO'
   else:
      repo_dir = None
      for repo_dir in (REPO_DIR.iterdir() if REPO_DIR.exists() else []):
         if remote:
            run_quiet(['git', 'fetch', remote[0], remote[1]], cwd=repo_dir)
         try:
            # check if the commit exists and canonicalize it
            proc = subprocess.run(['git', 'show', '-s', '--pretty="%H"', commit], cwd=repo_dir, capture_output=True, encoding='utf-8', check=True)
            commit = proc.stdout.strip()[1:-1]
            break
         except subprocess.CalledProcessError:
            repo_dir = None
            continue

      if repo_dir == None:
         print('Failed to find repository with commit: %s' % commit, file=sys.stderr)
         sys.exit(1)

      repo_name = repo_dir.name

   # setup build directory
   build_dir = BuildDir(buildtype, repo_name)
   build_dir.lock()

   # find prefix
   prefix_key = '\n'.join([str(commit), buildtype, config.get_key()])
   if options.prefix is not None:
      prefix = UnmanagedPrefix(Path(options.prefix).absolute())
   else:
      prefix = find_prefix(prefix_key, build_dir)

   # if we found a matching prefix, use that and skip build
   if commit and prefix.get_key() == prefix_key:
      return prefix

   try:
      cwd = os.getcwd()
      os.chdir(BASE_DIR) # we can't re-create the build directory if the cwd is the build directory
   except FileNotFoundError: # the current directory does not exist
      cwd = None

   # checkout
   if commit:
      run_quiet(['git', 'checkout', commit], cwd=repo_dir)

   if build_dir.get_config() != config.get_key():
      # re-create the build directory
      if not options.quiet:
         print('Creating build directory...', file=sys.stderr)

      rmtree(build_dir.directory)
      build_dir.directory.mkdir(parents=True, exist_ok=True)

      build_dir.set_config('') # if the setup fails, the build directory will be unusable
      run_quiet(['meson', 'setup', '--prefix', prefix.dir, '-Dbuildtype='+buildtype] + config.setup_args + [build_dir.directory, repo_dir], env=config.build_env)
   elif str(build_dir.get_prefix()) != str(prefix.dir):
      # correct the build directory's configured prefix
      if not options.quiet:
         print('Configuring new prefix...', file=sys.stderr)
      run_quiet(['meson', 'configure', '--prefix', prefix.dir] + config.setup_args + [build_dir.directory], env=config.build_env)

   build_dir.set_config(config.get_key())
   build_dir.set_prefix(prefix.dir)

   # build
   if options.quiet:
      run_quiet(['ninja'], cwd=build_dir.directory, env=config.build_env)
   else:
      print('Compiling...', file=sys.stderr)
      run_quiet(['ninja', 'build.ninja'], cwd=build_dir.directory, env=config.build_env) #configure
      proc = subprocess.run(['ninja'], cwd=build_dir.directory, env=config.build_env)
      if proc.returncode != 0:
         sys.exit(proc.returncode)

   # install
   if not isinstance(prefix, UnmanagedPrefix):
      rmtree(prefix.dir)
      prefix.dir.mkdir(parents=True, exist_ok=True)
   run_quiet(['ninja', 'install'], cwd=build_dir.directory, env=config.build_env, capture_output=True)

   if cwd:
      os.chdir(cwd)

   build_dir.unlock()

   prefix.set_key(prefix_key)
   return prefix

class RunEnv:
   def __init__(self, options):
      self.options = options
      self._prefix = None

   def __enter__(self):
      if self.options.prefix:
         prefix = UnmanagedPrefix(Path(self.options.prefix).absolute())
         env = {k:v for k,v in os.environ.items()}
      else:
         prefix = build_mesa(self.options)
         env = BUILD_CONFIGS[self.options.config].run_env

      # setup environment
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib/') + ':' + env.get('LD_LIBRARY_PATH', '')
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib/dri/') + ':' + env.get('LD_LIBRARY_PATH', '')
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib/gallium-pipe/') + ':' + env.get('LD_LIBRARY_PATH', '')
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib64/') + ':' + env.get('LD_LIBRARY_PATH', '')
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib64/dri/') + ':' + env.get('LD_LIBRARY_PATH', '')
      env['LD_LIBRARY_PATH'] = str(prefix.dir / 'lib64/gallium-pipe/') + ':' + env.get('LD_LIBRARY_PATH', '')
      icd_dir = prefix.dir / 'share/vulkan/icd.d/'
      if icd_dir.exists():
         env['VK_ICD_FILENAMES'] = ':'.join([str(fname) for fname in icd_dir.iterdir()])
         env['VK_DRIVER_FILES'] = env['VK_ICD_FILENAMES']
      env['PATH'] = str(prefix.dir / 'bin/') + ':' + env.get('PATH', '')
      env['VK_LAYER_PATH'] = str(prefix.dir / 'share/vulkan/implicit_layer.d') + ':' + env.get('VK_LAYER_PATH', '')
      env['VK_LAYER_PATH'] = str(prefix.dir / 'share/vulkan/explicit_layer.d') + ':' + env.get('VK_LAYER_PATH', '')
      ocl_icd_dir = prefix.dir / 'etc/OpenCL/vendors/'
      if ocl_icd_dir.exists():
         env['OCL_ICD_VENDORS'] = ocl_icd_dir
      env['GALLIUM_PIPE_SEARCH_DIR'] = str(prefix.dir / 'lib64/gallium-pipe/')

      if 'AMDGPU_GPU_ID' in env:
         env['LD_PRELOAD'] = str(prefix.dir / 'lib64/libamdgpu_noop_drm_shim.so') + ':' + env.get('LD_PRELOAD', '')
      if 'FD_GPU_ID' in env:
         env['LD_PRELOAD'] = str(prefix.dir / 'lib64/libfreedreno_noop_drm_shim.so') + ':' + env.get('LD_PRELOAD', '')
      if 'LIMA_DRM_SHIM' in env:
         env['LD_PRELOAD'] = str(prefix.dir / 'lib64/liblima_noop_drm_shim.so') + ':' + env.get('LD_PRELOAD', '')
      if 'INTEL_STUB_GPU_DEVICE_ID' in env or 'INTEL_STUB_GPU_PLATFORM' in env or 'INTEL_STUB_GPU' in env:
         # INTEL_STUB_GPU is an env var which can be used to LD_PRELOAD the drm shim and use the default
         env['LD_PRELOAD'] = str(prefix.dir / 'lib64/libintel_noop_drm_shim.so') + ':' + env.get('LD_PRELOAD', '')

      self._prefix = prefix

      return env

   def __exit__(self, exc_type, exc_value, traceback):
      self._prefix.unlock()

__all__ = ['DEFAULT_CONFIG', 'clean', 'add_repo', 'add_remote', 'build_mesa', 'Options', 'RunEnv']
