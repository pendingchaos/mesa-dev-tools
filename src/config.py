from pathlib import Path

BASE_DIR = Path(__file__).parent.parent / 'mesa-build'

with open(BASE_DIR / 'config.py', 'r') as f:
   config = {'BASE_DIR': BASE_DIR, 'c': {}}
   exec(f.read(), config)
   config = config['c']

__all__ = ['BASE_DIR', 'config']
