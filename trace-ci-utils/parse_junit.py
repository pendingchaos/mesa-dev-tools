#!/usr/bin/python3
# ./parse_junit.py <job id>
# Lists failures (like get_fails.py) and downloads images to /tmp/.
# iris-apl-traces outputs junit instead of piglit summary html, so get_fails.py doesn't work
import sys
import requests
import os
import os.path
import subprocess

job_id = int(sys.argv[1])
junit_url = 'https://mesa.pages.freedesktop.org/-/mesa/-/jobs/%s/artifacts/results/junit.xml' % job_id
junit_get = requests.get(junit_url)
assert(junit_get.status_code == 200)
junit = junit_get.text

def download_image(url, dest):
   os.makedirs(os.path.dirname('/tmp/' + dest), 0o777, True)

   get = requests.get(url)
   assert(get.status_code == 200)
   with open('/tmp/' + dest, 'wb') as f:
      f.write(get.content)

device = None
wrote = None
actual = None
expected = None
fail = None
fails = ''
downloads = ''
for line in junit.split('\n'):
   line = line.lstrip().rstrip()
   if '--device-name' in line:
      device = line.split('--device-name')[1].split(' ')[1].lstrip().rstrip()
   elif line.startswith('expected: '):
      expected = line.split(':')[1].lstrip()
   elif line.startswith('actual'):
      actual = line.split(':')[1].lstrip()
   elif line.startswith('Wrote '):
      wrote = line.removeprefix('Wrote ')
   elif line.startswith('Saving image at event ID'):
      wrote = line.split('to')[1].lstrip()
   elif line.startswith('[check_image] Images differ for:'):
      fail = True
   elif line.startswith('[check_image] Images match for:'):
      fail = False
   elif fail:
      fail = False
      fails += repr({'dev': device, 'trace': line, 'actual': actual, 'expected': expected}) + '\n'

      print(repr({'dev': device, 'trace': line, 'actual': actual, 'expected': expected}))
      print('Downloading images to /tmp/%s/%s' % (device, line))

      download_image('https://s3.freedesktop.org/mesa-tracie-results/mesa/mesa/%s.png' % expected,
                     '%s/%s/expected.png' % (device, line))
      actual_url = 'https://gitlab.freedesktop.org/mesa/mesa/-/jobs/%d/artifacts/raw' % (job_id)
      actual_url += wrote.removesuffix('.png')
      actual_url += '-' + actual + '.png?inline=false'
      download_image(actual_url, '%s/%s/actual.png' % (device, line))
      subprocess.run(['gm', 'compare', '-highlight-style', 'assign', '-highlight-color', 'purple', '-file',
                      '/tmp/%s/%s/diff.png' % (device, line),
                      '/tmp/%s/%s/expected.png' % (device, line),
                      '/tmp/%s/%s/actual.png' % (device, line)])
print()
with open('/tmp/junit_fails.txt', 'w') as f:
   f.write(fails)
   print('Wrote failures to /tmp/junit_fails.txt')
