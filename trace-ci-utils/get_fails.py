#!/usr/bin/python3
# ./get_fails.py <job id 1> <job id 2> ...
import sys
import requests

def parse_fail(url):
   trace = None
   actual = None
   expected = None

   get = requests.get(url)
   assert(get.status_code == 200)
   html = get.text
   trace_soon = False
   for line in html.split('\n'):
      line = line.lstrip().rstrip()
      if line == '[check_image] Images differ for:':
         trace_soon = True
      elif trace_soon:
         trace = line
         trace_soon = False
      if line.startswith('actual:'):
         actual = line.split(':')[1].lstrip().rstrip()
      elif line.startswith('expected:'):
         expected = line.split(':')[1].lstrip().rstrip()

   return trace, actual, expected

for job_id in sys.argv[1:]:
   job_id = int(job_id)
   url_prefix = 'https://mesa.pages.freedesktop.org/-/mesa/-/jobs/%d/artifacts/results/summary/' % job_id
   summary_problems_url = url_prefix + 'problems.html'
   summary_problems_get = requests.get(summary_problems_url)
   assert(summary_problems_get.status_code == 200)
   summary_problems_html = summary_problems_get.text

   fail = False
   prev_device = None
   for line in summary_problems_html.split('\n'):
      line = line.lstrip().rstrip()
      if line == '<td class="fail">':
         fail = True
      elif fail and line.startswith('<a href="results/trace@'):
         url = url_prefix + line.removeprefix('<a href="').removesuffix('">')
         line = line.split('@')
         device = url.split('@')[1]
         trace, actual, expected = parse_fail(url)

         assert prev_device == None or device == prev_device
         prev_device = device

         print(repr({'dev': device, 'trace': trace, 'actual': actual, 'expected': expected}))
      else:
         fail = False
