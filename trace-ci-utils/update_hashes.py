#!/usr/bin/python3
#./get_fails.py <job id 0> <job id 1> ... | ./update_hashes.py <mesa dir>
import sys
import os.path

def try_update_yml(yml, fail):
   with open(yml, 'r') as f:
      lines = list(f.read().split('\n'))

   res = []
   found_trace = False
   found_device = False
   done = False
   for line in lines:
      if line == ('  ' + fail['trace'] + ':'):
         found_trace = True;
      elif found_trace and line == ('    ' + fail['dev'] + ':'):
         found_device = True;
      elif line.startswith('      checksum: ') and found_trace and found_device and not done:
         assert(line.split(':')[1].lstrip() == fail['expected'])
         line = line.replace(fail['expected'], fail['actual'])
         done = True
      elif line.startswith('  ') and not line.startswith('    '):
         found_trace = False
         found_device = False
      elif line.startswith('    ') and not line.startswith('      '):
         found_device = False
      res.append(line)

   if not done:
      return False

   with open(yml, 'w') as f:
      f.write('\n'.join(res))

   return True

for fail in sys.stdin.read().split('\n'):
   if fail == '':
      continue
   print(fail)
   fail = eval(fail)
   done = False
   for yml in ['amd/ci/restricted-traces-amd.yml',
               'amd/ci/traces-amd.yml',
               'broadcom/ci/traces-broadcom.yml',
               'freedreno/ci/restricted-traces-freedreno.yml',
               'freedreno/ci/traces-freedreno.yml',
               'gallium/drivers/crocus/ci/traces-crocus.yml',
               'gallium/drivers/i915/ci/traces-i915.yml',
               'gallium/drivers/llvmpipe/ci/traces-llvmpipe.yml',
               'gallium/drivers/svga/ci/traces-vmware.yml',
               #no job uses this? it breaks updating traces-virgl because it also uses "gl-virgl"
               #'gallium/drivers/virgl/ci/traces-virgl-iris.yml',
               'gallium/drivers/virgl/ci/traces-virgl.yml',
               'gallium/drivers/zink/ci/traces-zink-restricted.yml',
               'gallium/drivers/zink/ci/traces-zink.yml',
               'gallium/frontends/lavapipe/ci/traces-lavapipe.yml',
               'intel/ci/traces-iris.yml',
               'panfrost/ci/traces-panfrost.yml']:
      if try_update_yml(os.path.join(sys.argv[1], 'src', yml), fail):
         done = True
         break
   assert(done)
   print('updated "%s" for %s' % (fail['trace'], fail['dev']))

print()
print('Remember to actually check that the images are correct.')
